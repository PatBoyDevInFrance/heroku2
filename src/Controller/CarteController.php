<?php

namespace App\Controller;

use App\Repository\ProductRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CarteController extends AbstractController
{
    #[Route('/carte', name: 'carte')]
    public function index(ProductRepository $repo): Response
    {   
        $carte = $repo->findAll();
        return $this->render('carte/carte.html.twig', [
            'cartes' => $carte
        ]);
    }
}
